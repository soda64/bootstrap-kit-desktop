
#!/bin/bash
# jako root

hostnamectl set-hostname crossfield0.wired.sytes.net

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

dnf install sudo vim mc ansible git tuned flatpak gedit-plugins gimp ansible-lint inkscape remmina qbittorrent samba vlc filezilla virt-manager wget tilix tilix-nautilus curl neofetch tree byobu htop libreoffice-base flameshot gnome-tweaks obs-studio

dnf update

groupadd admins
groupadd mods

usermod -a -G wheel soda2
usermod -a -G admins soda2
usermod -a -G libvirt soda2
usermod -a -G kvm soda2

# tuned-adm profile powersave

echo "%admins	ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/admins
echo "%mods	ALL=(ALL:ALL) ALL" > /etc/sudoers.d/mods

echo "#//192.168.1.1/g/storage /mnt/wired cifs uid=1000,credentials=/home/soda2/.smb,iocharset=utf8,x-systemd.requires=network.target,vers=2.0,sec=ntlmssp,noperm 0 0" >> /etc/fstab
